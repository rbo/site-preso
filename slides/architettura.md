# Architettura

NGINX > [Nuxt](https://nuxtjs.org/) > WP

--
## API
a wp possiamo fare delle domande e ci risponde in `JSON` tipo ad esempio dammi gli ultimi post:
https://wp.loc/wp-json/wp/v2/posts

--
### FrontEnd

- Chiede i dati al backend
- Riempe le viste

note: in questo talk frontend non significa che viene eseguito sul client
--
### Get data from BackEnd
<pre>
<code data-line-numbers="4-7|9-12">
const Site {

    // get posts
    getPosts () {
      return app.$http.$get('/wp-json/wp/v2/posts')
    }

    // get events
    getEvents () {
      return app.$http.$get('/wp-json/wp/v2/events')
    }

}
</code>
</pre>
notes: mi faccio una piccola libreria che torna gli elementi che gestisce il mio sito,
per fare questa libreria mi faccio aiutare da qualcosa tipo postman, insomnia facendomi le domande
ggiuste guardando i flussi
--
## Accortezze

- Paginazione
- Retrieve custom post type

notes: Non è sempre possibile dire a WP di restituirci gli elementi di cui
necessitiamo, bisogna convincerlo meglio e dargli una spintarella, ci sono vari plugin
per convincerlo a fare cose utili ma alla fine se vuoi le cose fatte bene tocca farsele.

--
## Tocca fare un plugin
custom type

```js

add_action('init', 'rbo_event_register_type');

function rbo_event_register_type () {
  $args = array(
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_rest' => true,
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'page',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 30,
    'menu_icon' => 'dashicons-calendar-alt',
    'page-attributes' => true,
    'show_in_nav_menus' => true,
    'taxonomies' => array('post_tag'),
    'show_in_admin_bar' => true,
    'show_in_menu' => true,
    'supports' => array('title','thumbnail','editor','page-attributes')
  ); 
  register_post_type( 'event', $args);

```

--
### altre vittorie

- sito statico, possiamo archiviare il sito in pure html!

