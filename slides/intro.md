> ## HEADLESS WORDPRESS

parole stampate senza testa

> a.k.a. come continuare ad usare wordpress nel 2021

--

### il problema
hai mille siti di cui vuoi rifare il frontend ma non vuoi impelagarti scrivendo un tema wp

--

### ma perchè ci teniamo wp?
- wordpress e' fatto per gli utenti ed e' anche per questo che e' diventato Il modo per pubblicare
- ma se ci vuoi mettere le mani e' un bagno di sangue
- richiede continue coccole (aggiornamenti, temi, plugin,...) e tu non ci stai più dentro
- ma il pannello di admin è conosciuto
- alla fine però il vero motivo è che CI SI DIVERTE A "PACIOCCARE"

--

### e soprattutto
... arriva quel momento nella vita, rifacciamo il sito di [Radio Blackout](https://radioblackout.org)

--
## Come?

L'idea è quella di continuare ad usare il backend (rimodellato sulle esigenze degli utenti) e il pannello di
amministrazione di WP (a cui tutti sono gia' abituatǝ) e sostituire il frontend con qualcosa di mantenibile
meglio nel 2021 (wp e' del 2007).
