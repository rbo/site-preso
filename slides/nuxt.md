### Nuxt.js

[Vue](https://vuejs.org) + SSR

--
### Vue
<pre>
<code data-line-numbers="2-4|7-12">
<div id='app'>
  <button @click='nClick++'>click {{nClick}}</button>
</div>

<script>
  new Vue({
    el: '#app',
    data: {
      nClick: 0
    }
  })
</script>
</code></pre>
<iframe src='iframe/vue.html'/>

--
## ehmmm

da qui in poi a braccio perche' il tempo per preparare le slides
e' terminato, lascio del link:

- https://nuxtjs.org/
- https://vuejs.org/
- repo del sito [WIP]: https://0xacab.org/rbo/site/
