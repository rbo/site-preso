# Indipendenza

> Come evitare di dover spegnere il telefono e fuggire dall'Italia
--

### Problemi riscontrati - Backend

L'attuale sito basato su un tema premium riscontra non pochi problemi nell'inserimento da parte dei redattori.
Un tema già pronto non risponderà mai completamente a delle esigenze troppo specifiche.

Note: troppi campi da compilare non necessari e difficoltà nell'utilizzo dei campi da utilizzare.
--

### Necessità

- Creare ad hoc i custom post types
- Mantenere solo i custom fields strettamente necessari e
- In generale semplificare l'inserimento di nuovi articoli e podcast
--

### Problemi riscontrati - Frontend 

Chi visita il sito ha bisogno di una navigazione semplice in cui poter trovare ciò che si sta cercando con una certà agilità. 
--
### Situazione attuale

https://radioblackout.org/
--
### Necessità

- Player senza interruzioni e con regolazione volume
- Migliorare la ricerca e gli archivi attraverso nuove suddivisioni tematiche
- Radunare tutti i contenuti informativi
- Migliorare la fruibilità dell'homepage
- Migliorare la visibilità di tutte le sezioni che attualmente risultano utili
- Sapere qual'è la trasmissione che si sta ascoltando 
--

### Nuovo wireframe

https://www.figma.com/proto/QT3EwGNAohlmThz75gNdIZ/RBO?node-id=0%3A1